from tinydb import TinyDB

class Graph:
    """A graph class used as data type to store parsed text"""

    def __init__(self):
        self._nodes = dict()
        _graph_db_file = 'graph.json'
        _graph_db = TinyDB(_graph_db_file)
        self._graph_table = _graph_db.table('Graph')

    def load(self):
        """Load graph from disk"""
        if self._graph_table.storage.read():
            self._nodes = self._graph_table.get(doc_id=1)

    def store(self, graph=None):
        """Store graph data

        :param graph: Graph object to store
        :return: None
        """
        graph = graph or self._nodes
        if self._graph_table.storage.read():
            self._graph_table.update(graph)
        else:
            self._graph_table.insert(graph)

    def add_node(self, node_name, *regular_edges, **custom_edges):
        """Add or modify graph node

        :param node_name: name of the new node
        :param regular_edges: a list of edges with the weight of 1
        :param custom_edges: a dict of edges (basically node) with custom weights
        """
        node_edges = self.get_node_edges(node_name)
        if node_edges:
            # node exists
            for edge in regular_edges:
                if edge in node_edges.keys():
                    node_edges[edge] += 1
                else:
                    node_edges[edge] = 1

            for edge, weight in custom_edges.items():
                node_edges[edge] = weight
        else:
            # node doesn't exist
            if regular_edges:
                self._nodes[node_name] = {edge: 1 for edge in regular_edges}
            elif custom_edges:
                self._nodes[node_name] = custom_edges

    def get_node_edges(self, node_name):
        return self._nodes[node_name] if node_name in self._nodes else None

    def show(self):
        from pprint import pprint
        pprint(self._nodes)


if __name__ == '__main__':
    g = Graph()
    print('Adding graph nodes...')
    g.add_node('a', b=3, c=7)
    g.add_node('b', c=4, a=1)
    g.add_node('c', b=5, a=9)
    g.add_node('d', 'a', 'b', 'c')
    # a = g.get_node_edges('a')
    g.show()
    print('Storing graph...')
    g.store()
    print('Restoring graph...')
    g.load()
    g.show()
