import argparse

from markov_chain_sentence_generator.parser import Parser


def main():
    parser = argparse.ArgumentParser(description='Generate sentences based on Markov chain principe')
    parser.add_argument('-f', '--file', metavar='FILE', type=str, nargs='+',
                        help='read text from FILE')
    args = parser.parse_args()

    if args.file:
        print('parsing from file: ' + ', '.join(args.file))
        p = Parser(args.file)
        p.parse()
        p.get_data()
    #TODO: Parsed data storage
    #TODO: Generate sentence based on graph node probability
    #TODO: Logging
    #TODO: Tests
    #TODO: README
    #TODO: Setup
    pass


if __name__ == '__main__':
    main()