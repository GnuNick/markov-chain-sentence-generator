from os.path import exists
from markov_chain_sentence_generator.graph import Graph


class Parser:
    """
    A text parser class to convert given text into Graph object.
    May accept files as raw text source.

    After file parsing action data can be stored or used in sentence generation procedure.
    """

    _RAW_TEXT = ''
    _GRAPH = Graph()

    def __init__(self, files=None):
        """
        Basic input checking, setting up for parsing.

        :param files: The list of files
        :type files: list
        """
        if files and isinstance(files, list):
            for file in files:
                assert exists(file), 'Provided file does not exist'
                self._get_file_content(file)

    def _get_file_content(self, file):
        """Get content from provided files

        :param file: file path
        :type file: str
        """
        with open(file, mode='r') as f:
            for line in f:
                self._RAW_TEXT += line

    def parse(self):
        """Convert raw text into Graph object data

        Parsing conventions:
            - Mechanism is ignoring punctuation marks inside provided sentences.
            - First word or first word of the sentence marks with 'START'.
            - Last word or last word of the sentence marks with 'END'.
        """
        raw_text = self._RAW_TEXT.split(' ')

        self._GRAPH.add_node('START', raw_text[0])
        self._GRAPH.add_node(raw_text[-1], 'END')

        for index, word in enumerate(raw_text):
            if index + 1 < len(raw_text):
                self._GRAPH.add_node(word, raw_text[index + 1])

    def store_data(self):
        """Store parsed data"""
        self._GRAPH.store()

    def restore_data(self):
        """Retrieve parsed data"""
        g = Graph()
        g.load()
        g.show()


if __name__ == '__main__':
    p = Parser(files=['test.txt'])
    p.parse()
    p.store_data()
    p.restore_data()
