# Markov chain sentence generator

A Python sentence generator based on the Markov chain.

## License

GNU General Public License v3.0 or later  
See [COPYING](COPYING) to see the full text.